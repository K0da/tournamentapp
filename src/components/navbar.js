import Link from "next/link";

export const Navbar = () => {
    return (
        <>
            <div className="navbar bg-base-100 drop-shadow-lg rounded-md">
                <div className="navbar-start">
                    <Link href="/" className="btn btn-ghost normal-case text-xl">Tournament App</Link>
                </div>
                <div className="navbar-center">
                    <ul className="menu menu-horizontal px-1">
                        <li> <Link href="/admin">Admin Page</Link> </li>
                        <li> <Link href="/elim1">E1</Link> </li>
                        <li> <Link href="/elim2">E2</Link> </li>
                        <li> <Link href="/elim3">E3</Link> </li>
                        <li> <Link href="/results">Results</Link> </li>
                    </ul>
                </div>
            </div>
        </>
    )
}
