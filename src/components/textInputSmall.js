export function TextInputSmall({placeholder, value, onChange}) {
    return (
        <div className="form-control w-full max-w-xs">
            <input type="text"
                   placeholder={placeholder}
                   className="input input-bordered input-sm w-full max-w-xs"
                   onChange={onChange}
                   value={value}/>
        </div>
    );
}