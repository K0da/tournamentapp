import PropTypes from "prop-types";

export function TextInput({placeholder, value, onChange}) {
    return (
        <div className="form-control w-full max-w-xs">
            <input type="text"
                   placeholder={placeholder}
                   className="input input-bordered w-full max-w-xs"
            onChange={onChange}
            value={value}/>
        </div>
    );
}

TextInput.propTypes = {
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
};

TextInput.defaultProps = {
    placeholder: '',
};
