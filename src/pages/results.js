import {Navbar} from "@/components/navbar";
import {useContext} from "react";
import {TeamContext} from "@/pages/_app";

export default function () {
    const {teams} = useContext(TeamContext)

    return (
        <>
            <Navbar/>
            <div className="mx-4 mt-4">
                <h1 className="text-3xl mb-8">Results</h1>
                {teams.map((team, index) => (
                    <div className="grid grid-cols-6 border-b-2">
                        <p className="my-auto">{team.name}</p>
                        <div className="grid grid-rows-2">
                            {team.players.map((player, index) => (
                                <p className="my-auto">{player + " " + team.name}</p>
                            ))}
                        </div>
                        <p className="my-auto ml-4">Trackmania</p>
                        <p className="my-auto ml-4">Super Animal Royale</p>
                        <p className="my-auto ml-4">Fall Guys</p>
                        <p className="my-auto ml-4">Total Points</p>
                    </div>
                    ))}
            </div>
        </>
    )
}