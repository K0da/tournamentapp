import '@/styles/globals.css'
import {createContext, useEffect, useState} from "react";
import Cookies from "js-cookie";

export const TeamContext = createContext(null);
const initialTeamState = {name: '', players: ['', '']};

export default function App({Component, pageProps}) {
    const [teamCount, setTeamCount] = useState(0);
    const [teams, setTeams] = useState(Array(1).fill(initialTeamState));

    useEffect(() => {
        // Load state from cookie
        const cookieState = loadStateFromCookie();
        if (cookieState) {
            setTeamCount(cookieState.teamCount);
            setTeams(cookieState.teams);
        }
    }, []);

    return (
        <TeamContext.Provider value={{teams, setTeams, teamCount, setTeamCount}}>
            <Component {...pageProps} />
        </TeamContext.Provider>
    )
}



export function teamsToString(teams) {
    return teams.map((team, index) => {
        const players = team.players.join(', ');
        return `Team ${index + 1}: ${team.name}, Players: ${players}`;
    }).join('\n');
}

export function saveStateToCookie(state) {
    Cookies.set('state', JSON.stringify(state), {expires: 1});
}

export function loadStateFromCookie() {
    const cookieState = Cookies.get('state');
    return cookieState ? JSON.parse(cookieState) : undefined;
}

export function resetStateCookie() {
    Cookies.remove('state');
}
