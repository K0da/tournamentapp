import React, {useContext} from "react";
import {TeamContext, teamsToString} from "@/pages/_app";
import {Navbar} from "@/components/navbar";
import {SERVER_FILES_MANIFEST} from "next/constants";
import {TextInputSmall} from "@/components/textInputSmall";

export default function Elimination1() {
    const {teams} = useContext(TeamContext)


    return (
        <>
            <Navbar/>
            <div className="mx-4 mt-4">
                <TeamDisplayWithMembers teams={teams}/>
            </div>
        </>
    )

}

function TeamDisplayWithMembers({teams}) {
    return (
        <>
            <h1 className="text-3xl mb-8">Trackmaina</h1>
            <div className="grid grid-cols-10 border-b-2">
                <p className="">Teamname</p>
                <p className="">Spielername</p>
                <p className="">Time Map 1</p>
                <p className="ml-4">Average Time</p>
                <p className="ml-4">Round Points</p>
                <p className="">Time Map2</p>
                <p className="ml-4">Average Time</p>
                <p className="ml-4">Round Points</p>
                <p className="ml-4">Total Points</p>
                <p className="ml-4">Ranking Points</p>
            </div>
            {teams.map((team, index) => (
                <div className="grid grid-cols-10 border-b-2">
                    <p className="my-auto">{team.name}</p>
                    <div className="grid grid-rows-2">
                        {team.players.map((player, index) => (
                            <p className="my-auto">{player + " " + team.name}</p>
                        ))}
                    </div>
                    <Results team={team} placeholder="mm:ss:xxx"/>
                    <Results team={team} placeholder="mm:ss:xxx"/>
                    <p className="my-auto ml-4">Total Points</p>
                    <p className="my-auto ml-4">Ranking Points</p>
                </div>
            ))}
        </>
    )
}

function Results({team, placeholder}) {
    return(
        <>
            <div className="grid grid-rows-2">
                {team.players.map((player, index) => (
                    <div>
                        <TextInputSmall placeholder={player + " Time " + placeholder}/>
                    </div>
                ))}
            </div>
            <p className="my-auto ml-4">Average Time</p>
            <p className="my-auto ml-4">Round Points</p>
        </>
    )
}



function DebugContext() {
    const {teams} = useContext(TeamContext)

    return (
        <>
            <p>{teamsToString(teams)}</p>
            <br/>
            <p>{teamCount}</p>
        </>
    )
}