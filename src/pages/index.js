import {Navbar} from "@/components/navbar";

export default function Home() {
  return (
    <>
        <Navbar/>
      <p className="m-4 text-6xl">Seas Havara!</p>
    </>
  )
}
