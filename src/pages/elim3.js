import {Navbar} from "@/components/navbar";
import {TeamContext} from "@/pages/_app";
import {useContext} from "react";
import {TextInput} from "@/components/textInput";

export default function Elimination3() {
    const {teams} = useContext(TeamContext)

    return (
        <>
            <Navbar/>
            <h1 className="text-3xl mb-8 mt-4 ml-4">Fall Guys</h1>
            <div className="mx-4">
                {teams.map((team, index) => (
                    <div className="grid grid-cols-9 border-b-2">
                        <p className="my-auto">{team.name}</p>
                        <div className="grid grid-rows-2">
                            {team.players.map((player, index) => (
                                <p className="my-auto">{player + " " + team.name}</p>
                            ))}
                        </div>
                        <TextInput placeholder="Points G1"/>
                        <TextInput placeholder="Points G2"/>
                        <TextInput placeholder="Points G3"/>
                        <TextInput placeholder="Points G4"/>
                        <TextInput placeholder="Points G5"/>
                        <p className="my-auto ml-4">Sum</p>
                        <p className="my-auto ml-4">Ranking Points</p>
                    </div>
                ))}
            </div>
        </>
    )
}