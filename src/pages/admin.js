import React, {useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Navbar} from "@/components/navbar";
import {TextInput} from "@/components/textInput";
import {resetStateCookie, saveStateToCookie, TeamContext} from "@/pages/_app";

const initialTeamState = {name: '', players: ['', '']};

export default function TeamManager() {
    const {teams, setTeams, teamCount, setTeamCount} = useContext(TeamContext)

    const updateTeam = (index, team) => {
        const newTeams = [...teams];
        newTeams[index] = team;
        setTeams(newTeams);
    };

    const handleTeamCountChange = (e) => {
        const value = parseInt(e.target.value);
        if (!isNaN(value) && value > 0 && value <= 100) {
            if (value > teams.length) {
                // If the new count is greater than the current count, add new teams
                const newTeams = Array(value - teams.length).fill(initialTeamState);
                setTeams(prevTeams => [...prevTeams, ...newTeams]);
            } else if (value < teams.length) {
                // If the new count is less than the current count, remove teams from the end
                setTeams(prevTeams => prevTeams.slice(0, value));
            }
            // Finally, update the teamCount state
            console.log(teamCount)
            setTeamCount(value);
        }
    };

    function save() {
        saveStateToCookie({teamCount, teams});
    }

    function reset() {
        resetStateCookie()
        window.location.reload();
    }

    return (
        <>
            <Navbar/>
            <div className="ml-4 mt-4">
                <TextInput
                    value={teamCount}
                    onChange={handleTeamCountChange}
                    placeholder="Enter number of teams"
                />
                <button className="btn btn-success mr-8" onClick={save}>Save</button>
                <button className="btn btn-error" onClick={reset}>RESET</button>
                <div className="flex flex-col flex-wrap">
                    {teams.map((team, index) => (
                        <TeamEntry
                            key={index}
                            team={team}
                            onTeamChange={(newTeam) => updateTeam(index, newTeam)}
                        />
                    ))}
                </div>
                <br/>
                <button className="btn btn-success mr-8" onClick={save}>Save</button>
                <button className="btn btn-error" onClick={reset}>RESET</button>
            </div>
        </>
    )
}

function TeamEntry({team, onTeamChange}) {
    const updateName = name => onTeamChange({...team, name});
    const updatePlayer = (index, name) => {
        const newPlayers = [...team.players];
        newPlayers[index] = name;
        onTeamChange({...team, players: newPlayers});
    };

    return (
        <>
            <div className="flex flex-row">
                <TeamNameInput value={team.name} onChange={updateName}/>
                <PlayerList players={team.players} onPlayerChange={updatePlayer}/>
            </div>
            <div className="divider"/>
        </>
    );
}

TeamEntry.propTypes = {
    team: PropTypes.shape({
        name: PropTypes.string,
        players: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
    onTeamChange: PropTypes.func.isRequired,
};

function TeamNameInput({value, onChange}) {
    return (
        <div className="flex flex-col justify-center">
            <TextInput
                placeholder="Team Name"
                value={value}
                onChange={e => onChange(e.target.value)}
            />
        </div>
    );
}

TeamNameInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
};

function PlayerList({players, onPlayerChange}) {
    return (
        <div className="flex flex-col">
            {players.map((player, index) => (
                <TextInput
                    key={index}
                    placeholder={`Player ${index + 1}`}
                    value={player}
                    onChange={e => onPlayerChange(index, e.target.value)}
                />
            ))}
        </div>
    );
}


PlayerList.propTypes = {
    players: PropTypes.arrayOf(PropTypes.string).isRequired,
    onPlayerChange: PropTypes.func.isRequired,
};
