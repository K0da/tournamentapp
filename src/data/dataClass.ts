class Player {
    points: number = 0;

    constructor(public playerName: string) {
    }

}

class Team {
    constructor(public teamName: string,
                public player1: Player,
                public player2: Player) {}

    getTotalPoints() {
        return this.player1.points + this.player2.points;
    }
}

class Game {
    allPlayers: [string];
}

class Elim1 {
    x: {};


}